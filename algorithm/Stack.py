import unittest

class Node:
    def __init__(self, value = None):
        self.prev = None
        self.next = None
        self.value = value
    def __str(self):
        return self.value

class StackBase:
    def pop(self):
        raise NotImplemented
    def push(self, value):
        raise NotImplemented
    def peek(self):
        raise NotImplemented
    def isEmpty(self):
        raise NotImplemented
    def display(self):
        raise NotImplemented

class LinkedStack(StackBase):

    def __init__(self):
        self.head = None
        self.size = 0

    def push(self, value):
        if(self.head == None):
            self.head = Node(value)
        else:
            newNode = Node(value)
            temp = self.head
            newNode.next = temp
            self.head = newNode
        self.size = self.size + 1

    def pop(self):
        if self.head == None:
            return -1
        popNode = self.head
        self.head = popNode.next
        self.size = self.size - 1

        return popNode.value

    def peek(self):
        return self.head

    def isEmpty(self):
        return self.size == 0

    def display(self):
        tempNode = self.head
        results = []
        while tempNode != None:
            results.append(tempNode.value)
            tempNode = tempNode.next
        print(results)

class StackTeat(unittest.TestCase):

    def test(self):
       stack = LinkedStack()
       stack.push(1)
       stack.push(2)
       stack.display()
       self.assertEqual(stack.pop(), 2)
       self.assertEqual(stack.pop(), 1)
       stack.push(3)
       stack.push(5)
       stack.push(7)
       stack.display()