import unittest

class Stack:

    def __init__(self):
        self.items = []

    def pop(self):
        if(self.isEmpty()):
            return None
        return self.items.pop()

    def push(self,value):
        self.items.append(value)

    def peek(self):
        return self.items[len(self.items)-1 ]

    def isEmpty(self):
        return self.size() == 0

    def size(self):
        return len(self.items)


class SetOfStacks:

    def __init__(self, max):
        self.stacksMax = max
        self.stackList = []
        self.stackList.append(Stack())

    def push(self, value):
        if (self.lastStack().size() >= self.stacksMax):
            self.stackList.append(Stack())
        self.lastStack().push(value)

    def pop(self):

        if(self.lastStack().isEmpty()):
            self.stackList.pop()
        return self.lastStack().pop()

    def print(self):
        result = []
        for stack in self.stackList:
            for item in stack.items:
                result.append(item)
        return result

    def lastStack(self):
        return self.stackList[len(self.stackList) - 1]

    def len(self):
        return len(self.stackList)

    def isEmpty(self):
        return self.len() == 0


class SetOfStacksTest(unittest.TestCase):

        def test(self):
            stacks = SetOfStacks(3)
            print('stack 개수',stacks.len())
            stacks.push(1)
            stacks.push(2)
            stacks.push(3)
            stacks.push(4)
            print('stack 개수', stacks.len())
            stacks.push(5)
            stacks.push(6)
            stacks.push(7)
            print(stacks.print())
            print('stack 개수',stacks.len())
            print('pop', stacks.pop())
            print('pop', stacks.pop())
            print('stack 개수',stacks.len())
            print('pop', stacks.pop())
            print('pop', stacks.pop())
            print('pop', stacks.pop())
            print(stacks.print())
            print('pop', stacks.pop())
            print('stack 개수', stacks.len())