import unittest

class Node:

    def __init__(self,item):
        self.value = item
        self.left = None
        self.right = None

class BinaryTree:

    def __init__(self):
        self.head = Node(None)

        self.preorder_list = []
        self.inorder_list = []
        self.postorder_list = []

    def add(self, item):

        if(self.head.value == None):
            self.head.value = item
        else:
            self.addNode(self.head,item)

    def addNode(self,cur,item):

        if(cur.value >= item):
            if(cur.left == None):
                cur.left = Node(item)
            else:

                self.addNode(cur.left, item)
        else:
            if(cur.right == None):
                cur.right = Node(item)
            else:
                self.addNode(cur.right, item)

    def checkHeight(self, node):

        if(node is None):
            return 0

        leftHeight = self.checkHeight(node.left)
        if(leftHeight == -1):
            return -1

        rightHeight = self.checkHeight(node.right)
        if (rightHeight == -1):
            return -1

        heightDiff = leftHeight - rightHeight

        if(abs(heightDiff) > 1):
            return -1
        else:
            return max(leftHeight,rightHeight) +1

    def isBalanced(self):
        if(self.checkHeight(self.head) == -1):
            return False
        else:
            return True




class BinaryTreeIsBalanceTest(unittest.TestCase):

    def test(self):
        binayTree = BinaryTree()
        binayTree.add(15)
        binayTree.add(10)
        binayTree.add(9)
        self.assertFalse(binayTree.isBalanced())
        binayTree.add(18)
        binayTree.add(19)
        self.assertTrue(binayTree.isBalanced())



