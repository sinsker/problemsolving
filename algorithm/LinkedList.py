import unittest

class Node:
    def __init__(self, value):
        self.next = None
        self.value = value
    def __str(self):
        return self.value

class LinkedList:

    def __init__(self):
        self.head = None
        self.tail = self.head
        self.current = None
        self.size = 0

    def  add(self, key = None, value = None):

        if(key != None and key >= self.size):
            return -1

        newNode = Node(value)
        if( self.head == None):
            self.head = newNode
        else:
            if(key != None and key != 0):
                temp = self.head
                prevNode = None
                for i in range(key):
                    prevNode = temp
                    temp = temp.next
                newNode.next = temp
                prevNode.next = newNode
            else:
                temp = self.head
                newNode.next = temp
                self.head = newNode
        self.size = self.size + 1

    def get(self, key):
        self.current = self.head
        for i in range(key):
            if(self.current == None):
                return -1
            self.current = self.current.next
        return self.current.value

    def remove(self, key):
        curNode = self.head
        prevNode = None

        if(self.size-1 <= key):
            return -1

        for i in range(key):
            prevNode = curNode
            curNode = curNode.next

        if( prevNode == None):
            self.head = curNode.next
        else:
            nextNode = curNode.next
            prevNode.next = nextNode
            curNode = None

    def all(self):
        results = []
        self.current = self.head
        while(self.current != None):
            results.append(self.current.value)
            self.current = self.current.next
        return results

class LinkedListTest(unittest.TestCase):

    def test(self):
        list = LinkedList()
        list.add(value=1)
        list.add(value=2)
        list.add(value=3)
        list.add(key=1,value=5)
        list.add(key=2, value=7)
        print(list.all())
        list.remove(3)
        list.remove(3)
        print(list.all())
        print(list.get(2))