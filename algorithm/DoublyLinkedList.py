import unittest

class Node:
    def __init__(self, value):
        self.value = value
        self.prev = None
        self.next = Node


class DoublyLinkedList:

    def __init__(self):
        self.head = Node(None)
        self.tail = Node(None)
        self.head.next = self.tail
        self.tail.prev = self.head
        self.size = 0

    def push(self, value):
        newNode = Node(value)
        tempNode = self.head.next
        newNode.next = tempNode
        tempNode.prev = self.head
        self.head.next = newNode
        self.size += 1

    def pop(self):
        if(self.size == 0):
            return -1
        node = self.head.next
        nextNode = node.next
        nextNode.prev = self.head
        self.head.next = nextNode
        self.size -= 1
        return node.value

    def add(self, value):
        newNode = Node(value)
        tempNode = self.tail.prev
        newNode.prev = tempNode
        newNode.next = self.tail
        tempNode.next = newNode
        self.tail.prev = newNode
        self.size += 1

    def get(self, key):
        node = self.search(key)
        if(node == -1):
            return -1
        return node.value

    def remove(self, key):
        node = self.search(key)
        if (node == -1):
            return -1
        prevNode = node.prev
        nextNode = node.next
        prevNode.next = nextNode
        nextNode.prev = prevNode
        node = None
        self.size -= 1

    def search(self,key):
        if (self.size == 0 or self.size <= key):
            return -1
        node = self.head
        cnt = key
        i = 0
        while (i < cnt + 1):
            node = node.next
            i += 1
        return node

    def searchIndex(self, value):
        if (self.size == 0):
            return -1
        node = self.head.next
        index = 0
        while (node != self.tail):
            if(value == node.value):
                return index
            index += 1
            node = node.next
        return -1

    def searchNode(self, node):
        if (self.size == 0):
            return -1
        temp = self.head.next
        index = 0
        while (temp != self.tail):
            if(node == temp):
                return temp
            index += 1
            node = temp.next
        return -1

    def list(self):
        result = []
        node = self.head.next
        while(node != self.tail):
            result.append(node.value)
            node = node.next
        return result

    def blist(self):
        result = []
        node = self.tail.prev
        while (node != self.head):
            result.append(node.value)
            node = node.prev
        return result


class DoublyLinkedListTest(unittest.TestCase):

    def test(self):
        linkedList = DoublyLinkedList()

        linkedList.add(1)
        linkedList.add(2)
        linkedList.add(3)
        print(linkedList.list())
        linkedList.remove(1)
        print(linkedList.list())
        linkedList.add(4)
        print(linkedList.list())
        print(linkedList.get(1))
        print(linkedList.pop())
        print(linkedList.list())
        print(linkedList.blist())


