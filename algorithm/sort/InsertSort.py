import unittest

def insertSort(array):

    for i in range(len(array) ):
        temp_i = i
        while(temp_i > 0 and array[temp_i-1] > array[temp_i] ):
            array[temp_i-1], array[temp_i] = array[temp_i], array[temp_i-1]
            temp_i = temp_i - 1
    return array


class InsertSortTest(unittest.TestCase):

    def test(self):
        array = [3,4,1,6,2,5]
        result =insertSort(array)
        self.assertEqual([1,2,3,4,5,6], result)
