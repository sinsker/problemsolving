'''
quick sort ::
1. pivot 을 기준으로 왼쪽은 pivot 보다 크거나 같은값 오른쪽은 pivot 보다 작은 값을 찾아 swap 시킨다.
2. 둘중 하나만 찾고 나머지가 만나게 되면 만난값과 pivot을 swap 시킨다.

시간 복잡도 : O(nLogn)
최악일경우 : O(n^2)
'''
import unittest

def quickSort(alist, start, end):

    if(start < end):
        p = partition(alist,start,end)
        quickSort(alist,start, p - 1 )
        quickSort(alist, p + 1, end)

def partition(arr, start, end):

    pivot = end
    well = start
    left = start

    while(pivot > left):
        if(arr[pivot] > arr[left]):
            arr[well], arr[left] = arr[left], arr[well]
            well += 1
        left += 1

    arr[well], arr[pivot] = arr[pivot], arr[well]
    return well


class QuickSortTest(unittest.TestCase):

    def test(self):
        list = [123, 32, 12, 54, 6, 123, 43, 650, 1, 8]
        quickSort(list, 0, len(list) - 1)
        self.assertEqual(list,[1, 6, 8, 12, 32, 43, 54, 123, 123, 650])






