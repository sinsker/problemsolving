import unittest


def bubbleSort(array):

    for i in range(len(array) - 1):
        for j in range(len(array) -1):

            if(array[j] > array[j+1]):
                array[j+1], array[j] = array[j], array[j+1]
    return array


class BubbleSortTest(unittest.TestCase):

    def test(self):
        array = [3,4,1,6,2,5]
        result =bubbleSort(array)
        self.assertEqual([1,2,3,4,5,6], result)
