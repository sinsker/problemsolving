import unittest

def selectionSort(array):

    for i in range(len(array) -1 ):
        temp_i = i+1
        min = i

        while( temp_i < len(array)):
            if( array[temp_i] < array[min] ):
                min = temp_i
            temp_i = temp_i + 1

        if min is not i:
            array[i],array[min] = array[min],array[i]

    return array

class SelectionSortTest(unittest.TestCase):

    def test(self):
        array = [3,4,1,6,2,5]
        result =selectionSort(array)
        self.assertEqual([1,2,3,4,5,6], result)
