'''
heap sort ::
1. 주어진 배열을 최대 힙트리로 구성한다.
2. 최대 힙트리 구성 후 부모노드와 가장 마지막 index의 값과 swap 시킨다.
3. 정렬 될때까지 1,2 반복

시간 복잡도 : O(nLogn)
'''

import unittest

def heapSort(list):

    def swap(list, a, b):
        list[a],list[b] = list[b],list[a]

    def heapify(list,size):
        p = int(size / 2) - 1
        while(p >= 0):
            siftdown(list, p, size)
            p -= 1

    def siftdown(list, p, size):
        L = int(2 * p) + 1
        R = int(2 * p) + 2
        max = p
        if(L <= size-1 and list[L] > list[max]):
            max = L
        if(R <= size-1 and list[R] > list[max]):
            max = R
        if(max != p):
            swap(list, max, p)
            siftdown(list, max, size)

    size = len(list)
    heapify(list, size)
    end = size-1
    while(end > 0):
        swap(list, 0, end)
        siftdown(list, 0, end)
        end -=1

    return list


class HeapSortTest(unittest.TestCase):

    def test(self):
        array = [3, 4, 1, 6, 2, 5]
        result = heapSort(array)
        self.assertEqual([1, 2, 3, 4, 5, 6], result)
