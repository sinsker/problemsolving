import unittest
from .SetOfStacks import Stack

class QueueOfTowStacks:

    def __init__(self):
        self.stack1 = Stack()
        self.stack2 = Stack()

    def enqueue(self, item):
        self.stack1.push(item)

    def dequeue(self):
        if(self.stack2.isEmpty()):
            while self.stack1.isEmpty() == False:
                self.stack2.push(self.stack1.pop())
        return self.stack2.pop()


class QueueOfTowStacksTest(unittest.TestCase):

    def test(self):
        queue = QueueOfTowStacks()
        queue.enqueue(1)
        queue.enqueue(2)
        queue.enqueue(3)
        print('dequeue',queue.dequeue())
        print('dequeue', queue.dequeue())
        queue.enqueue(4)
        queue.enqueue(5)
        print('dequeue', queue.dequeue())
        print('dequeue', queue.dequeue())
        print('dequeue', queue.dequeue())
