from .DoublyLinkedList import DoublyLinkedList
import unittest

class LRUCache():

    def __init__(self, cacheSize):
        self.dict = {}
        self.cache = DoublyLinkedList()
        self.frame  = cacheSize

    def set(self,key, value):
        if key in self.dict:
            index = self.cache.searchIndex(key)
            self.cache.remove(index)

        self.dict[key] = value
        self.cache.add(key)

        if(self.cache.size > self.frame):
            node = self.cache.pop()
            del self.dict[node]

    def get(self, key):
        if key in self.dict:
            value = self.dict[key]
            index = self.cache.searchIndex(key)
            self.cache.remove(index)
            self.cache.add(key)
            return value
        else:
            return -1
    def cacheStack(self):
        return self.cache.list()

class LRUCacheTest(unittest.TestCase):

    def test(self):
        lru = LRUCache(3)
        lru.set('kim','김')
        lru.set('lee', '이')
        print(lru.cacheStack())
        lru.set('kim', '수')
        print(lru.cacheStack())
        print(lru.get('lee'))
        print(lru.cacheStack())

        lru.set('pack', '박')
        print(lru.cacheStack())

        lru.set('bu', '부')
        print(lru.cacheStack())

