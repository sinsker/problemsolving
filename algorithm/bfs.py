import unittest

class bfsClass:

    def __init__(self,vertexList, edgeList):
        self.vertexList = vertexList
        self.edgeList = edgeList

    def play(self, start):
        visitedList = []
        queue = [start]
        adjacencyList = [ [] for vertex in self.vertexList]


        for edge in self.edgeList:
            adjacencyList[edge[0]].append(edge[1]) #edge는 단방향이므로 위치별 인접노드들 저장

        print(adjacencyList)

        while queue:
            current = queue.pop() #시작 위치
            for neighbor in adjacencyList[current]: #노드별 접근 가능 노드위치
                if(not neighbor in visitedList):
                    queue.insert(0,neighbor)  # 접근가능 노드위치 queue에 삽입

            visitedList.append(self.vertexList[current]) # queue에 삽입되면 접근한 노드로 인지

        return visitedList



class bfsTest(unittest.TestCase):

    def test(self):
        vertexList = ['A', 'B', 'C', 'D', 'E', 'F', 'G']
        edgeList = [(0, 1), (1, 2), (1, 3), (3, 4), (4, 5), (1, 6)]

        bfs = bfsClass(vertexList, edgeList)
        visit = bfs.play(0)
        print(visit)

