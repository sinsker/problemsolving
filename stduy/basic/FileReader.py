import pickle
import re

class FileReader:

    def __init__(self,path = ''):
        self.path = path

    def __del__(self):
        pass


    def read(self):
        try:
           file = open(self.path,'r')
           data = file.readlines()
           for line in data:
               print(line)
        except:
            print("파일이 존재하지 않습니다", self.path)
        finally:
            file.close()

    def update(self, text):
        try:
            with open(self.path, 'a') as file:
                file.write(text+"\n")
        except:
            print("파일이 존재하지 않습니다", self.path)

    def write(self,text):
        try:
            with open(self.path, 'w') as file:
                file.write(text)
        except:
            print("파일이 존재하지 않습니다", self.path)

    def replace(self, origin, replace):
        try:
            file = open(self.path, 'r')
            txt = file.read()
            txt = re.sub(origin, replace, txt)
            self.write(txt)

        except:
            print("파일이 존재하지 않습니다", self.path)
        finally:
            file.close()


if __name__ == "__main__":
    fileReader = FileReader('D:/workspaces/hadoop/170725/test.txt')
    fileReader.replace('로','고')
    fileReader.read()