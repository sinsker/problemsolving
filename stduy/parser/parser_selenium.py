import requests
from bs4 import BeautifulSoup
import re
import json
import os

class WebParserLink:

    def __init__(self):
        self.link = []

    def loop(self,href):
        html = self.soup(href)

        for a_text in html.select('a'):
            url = a_text.get('href')
            #print(m)
            if (bool(re.search('^http*',url))):
                if(self.search(url) == 0):
                    self.link.append(url)
                    #self.loop(href)

    def search(self,text):
        if any(text in s for s in self.link):
            return 1
        return 0

    def soup(self,href):
        req = requests.get(href)
        req_text = req.text
        return  BeautifulSoup(req_text, 'html.parser')

    def result(self):
        print(self.link)
        with open(os.path.join(os.path.dirname(__file__),'file.json'),'+w') as file:
            json.dump(self.link,file)


parser = WebParserLink()
parser.loop('http://www.champstudy.com/')
parser.result()