import requests
from bs4 import BeautifulSoup
import json


def parse_url(href,selecter):
    req = requests.get(href)
    if(req.status_code != 200):
        return False

    html = req.text
    soup = BeautifulSoup(html, 'html.parser')

    html_text = soup.select(selecter)
    data  = {}
    for text in html_text:
        data[text.text] = text.get('href')

    return data

def parse_blog():
    req = requests.get('https://beomi.github.io/beomi.github.io_old/')
    html = req.text
    soup = BeautifulSoup(html, 'html.parser')
    my_titles = soup.select(
        'h3 > a'
        )
    data = {}
    for title in my_titles:
        data[title.text] = title.get('href')
    return data


if __name__ == "__main__":

    #BAS_PATH = os.path.dirname(os.path.abspath(__file__))


    # HTTP GET Request
    # req = requests.get('https://beomi.github.io/beomi.github.io_old/')
    #html = req.text
    # HTML 소스 가져오기
    #html = req.text
    # HTTP Header 가져오기
    #header = req.headers
    # HTTP Status 가져오기 (200: 정상)
    #status = req.status_code
    # HTTP가 정상적으로 되었는지 (True/False)
    #is_ok = req.ok

    #soup = BeautifulSoup(html, 'html.parser')
    #my_title = soup.select('h3 > a')

    #data = {}

    #for title in my_title:
     #   data[title.text] = title.get('href')

    # with open(os.path.join(BAS_PATH, 'result.json'), 'w+') as json_file:
    #    json.dump(data, json_file)

    link_data_dict = parse_url("http://www.champstudy.com/?r=champstudy&c=free/free_lecture","#language > ul > li > a")
    for t, l in link_data_dict.items():
        pass