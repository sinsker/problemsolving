import requests
from bs4 import BeautifulSoup
import json
import os

BASH_PATH = os.path.dirname(os.path.abspath(__file__))

LOGIN_INFO = {
    'userId': 'myidid',
    'userPassword': 'mypassword123'
}

data = {}

with requests.Session() as s:
    csrf_page = s.get('https://www.clien.net/service')
    html = csrf_page.text
    soup = BeautifulSoup(html,'html.parser')
    csrf = soup.find('input', {'name':'_csrf'}).get('value')

    LOGIN_INFO['_csrf'] = csrf
    # HTTP GET Request: requests대신 s 객체를 사용한다.
    login_req = s.post('https://www.clien.net/service/login', LOGIN_INFO)

    if(login_req.status_code != 200):
        raise Exception('로그인이 되지 않았습니다. 아이디와 비밀번호를 확인해 주세요.')

    post_one = s.get('https://www.clien.net/service/board/rule/10707408')
    soup = BeautifulSoup(post_one.text, 'html.parser')
    title = soup.select('#div_content > div.post-title > h3 > div')
    contents = soup.select('#div_content > div.post.box > div.post-content')

    data['title'] = title[0].text
    data['contents'] = contents[0].text
    with open(os.path.join(BASH_PATH,'parser_session.json'), 'w+') as result:
        json.dump(data,result)